using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PointCounter : MonoBehaviour
{
    [Inject] private SignalBus m_SignalBus;
    
    [SerializeField] private int m_iRawPoints = 0;
    [SerializeField] private int m_iMultiplier = 1;
    
    #region Signals

    public class PointsChangedSignal
    {
        private int m_iPoints;
        
        public PointsChangedSignal(int _iPoints)
        {
            iPoints = _iPoints;
        }

        public int iPoints { get; }
    }
    
    public class PointsAddedSignal
    {
        public PointsAddedSignal(int _iPoints)
        {
            iPoints = _iPoints;
        }

        public int iPoints { get; }
    }
    
    public class MultiplierChangedSignal
    {
        public MultiplierChangedSignal(int _iMultiplier)
        {
            iMultiplier = _iMultiplier;
        }

        public int iMultiplier { get; }
    }
    
    public class MultiplierAddedSignal
    {
        public MultiplierAddedSignal(int _iMultiplier)
        {
            iMultiplier = _iMultiplier;
        }

        public int iMultiplier { get; }
    }

    #endregion

    //Get the points multiplied by the current multiplier
    public int iPoints
    {
        get { return m_iRawPoints * m_iMultiplier; }
    }

    //Get the points without the multiplier applied
    public int iRawPoints
    {
        get => m_iRawPoints;
    }

    public int iMultiplier
    {
        get => m_iMultiplier;
    }

    private void Awake()
    {
        m_SignalBus.Subscribe<PointsAddedSignal>(OnPointsAdded);
        m_SignalBus.Subscribe<MultiplierAddedSignal>(OnMultiplierAdded);
        m_SignalBus.Subscribe<GameOverUI.RestartSignal>(OnRestart);
    }

    private void OnPointsAdded(PointsAddedSignal _pointsAddedSignal)
    {
        m_iRawPoints += _pointsAddedSignal.iPoints;
        
        m_SignalBus.Fire(new PointsChangedSignal(m_iRawPoints));
    }

    private void OnMultiplierAdded(MultiplierAddedSignal _multiplierAddedSignal)
    {
        m_iMultiplier += _multiplierAddedSignal.iMultiplier;
        
        m_SignalBus.Fire(new MultiplierChangedSignal(m_iMultiplier));
    }

    private void OnRestart()
    {
        m_iRawPoints = 0;
        m_iMultiplier = 1;
        
        m_SignalBus.Fire(new PointsChangedSignal(m_iRawPoints));
        m_SignalBus.Fire(new MultiplierChangedSignal(m_iMultiplier));
    }
}
