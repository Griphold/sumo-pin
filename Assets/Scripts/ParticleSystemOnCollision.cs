using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Collider2D))]
public class ParticleSystemOnCollision : MonoBehaviour
{
    [SerializeField] private GameObject m_goParticleSystemPrefab;
    [SerializeField] private AnimationCurve m_SpeedByImpulse;
    [SerializeField] private float m_fMaxImpulse = 50;

    private void OnCollisionEnter2D(Collision2D _colli2DOther)
    {
        ContactPoint2D contact = _colli2DOther.GetContact(0);
        GameObject goNewParticles = Instantiate(m_goParticleSystemPrefab, contact.point, Quaternion.LookRotation(contact.normal, Vector3.back));
        
        //Slow down particles for slow collisions
        float fClampedImpulse = Mathf.Clamp(contact.normalImpulse, 1, m_fMaxImpulse); //Clamp here so designer can adjust curve between 0 and 1
        float fImpulseFactor = m_SpeedByImpulse.Evaluate(fClampedImpulse / m_fMaxImpulse);

        //Slightly Hacky solution
        //Can't be bothered to read up on the new particle system. I'm sure this works much easier in the new system. 
        ParticleSystem particleSystem = goNewParticles.GetComponent<ParticleSystem>();
        particleSystem.startSpeed = particleSystem.main.startSpeed.constantMax * fImpulseFactor;
    }
}
