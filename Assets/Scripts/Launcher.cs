using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Zenject;

public class Launcher : MimiBehaviour
{
    [Inject] private SignalBus m_SignalBus;
    
    [SerializeField] private SpringJoint2D m_springLauncher;
    [SerializeField] private float m_fLaunchStrength;
    [SerializeField] private float m_fCompressSpeed;
    [SerializeField] private AnimationCurve m_CompressByDistanceCurve;

    private Transform m_transLaunchPoint;
    private Rigidbody2D m_rigidPlunger;
    private bool m_bStarted = false;

    protected override void Awake()
    {
        base.Awake();
        
        //Set up launcher
        m_transLaunchPoint = m_springLauncher.gameObject.transform; //The point to where the plunger is pushed
        m_rigidPlunger = m_springLauncher.connectedBody; //The rigidbody of the plunger
        m_springLauncher.distance = m_fLaunchStrength; //The strength of the spring
        Vector3 setupPosition = m_transLaunchPoint.TransformPoint(0, m_fLaunchStrength, 0); 
        m_rigidPlunger.transform.position = setupPosition; //move the plunger so the spring is compressed
        
        //Subscribe to input events
        m_SignalBus.Subscribe<PlayerInput.FireActionSignal>(OnStartLaunch);
        m_SignalBus.Subscribe<PlayerInput.FireActionReleasedSignal>(OnLaunch);
    }

    private void FixedUpdate()
    {
        if (m_bStarted)
        {
            //calculate distance from launch point
            Vector2 v2LaunchPoint = m_transLaunchPoint.position;
            float fDistance = Vector2.Distance(v2LaunchPoint, m_rigidPlunger.position);

            //the further we are away from the launch point the slower the plunger moves
            float fDistanceFactor = Mathf.Clamp01(fDistance / m_fLaunchStrength);
            float fCurveFactor = m_CompressByDistanceCurve.Evaluate(1 - fDistanceFactor);

            Vector2 velocity = m_transLaunchPoint.TransformDirection(Vector2.down) * (m_fCompressSpeed * fCurveFactor);
            
            if (fDistance > 0.25f)
            {
                m_rigidPlunger.position += velocity * Time.fixedDeltaTime;
                
            }
        }
    }

    private void OnStartLaunch()
    {
        //Rigidly move the launcher
        m_bStarted = true;
        m_rigidPlunger.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
    }

    private void OnLaunch()
    {
        //Do not freeze the y position anymore so the spring is released
        m_bStarted = false;
        m_rigidPlunger.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
    }
}
