using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallAnimator : MonoBehaviour
{
    //Assigned Members
    [SerializeField] private Animator m_AnimatorThis; 
    [SerializeField] private float m_fAnimationMultiplier;

    //Private Members
    private Rigidbody2D m_rigidThis;
    
    private void Awake()
    {
        m_rigidThis = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        float fSpeed = m_rigidThis.velocity.magnitude;


        m_AnimatorThis.speed = fSpeed * m_fAnimationMultiplier;
    }
}
