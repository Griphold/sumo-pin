using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using Zenject;

public class Paddle : MimiBehaviour
{
    [Inject] private SignalBus m_SignalBus;

    [System.Serializable]
    public enum Type
    {
        Left,
        Right
    };

    //Exposed members
    [SerializeField]
    private Type m_eType = Type.Left;

    [SerializeField]
    private float m_fSpeed = 1500;
    
    //Private members
    private float m_fOriginalRotation;
    private HingeJoint2D m_HingeJoint2D;
    private JointMotor2D m_JointMotor2D;

    protected override void Awake()
    {
        base.Awake();

        m_HingeJoint2D = m_transThis.GetComponent<HingeJoint2D>();
        m_JointMotor2D = m_HingeJoint2D.motor;

        //subscribe to the appropriate signals
        switch (m_eType)
        {
            case Type.Left:
                m_SignalBus.Subscribe<PlayerInput.LeftActionSignal>(OnAction);
                m_SignalBus.Subscribe<PlayerInput.LeftActionReleasedSignal>(OnReleased);
                break;
            
            case Type.Right:
                m_SignalBus.Subscribe<PlayerInput.RightActionSignal>(OnAction);
                m_SignalBus.Subscribe<PlayerInput.RightActionReleasedSignal>(OnReleased);
                break;
        }
        
        OnReleased();
    }

    //Action
    private void OnAction()
    {
        m_JointMotor2D.motorSpeed = m_fSpeed;
        m_HingeJoint2D.motor = m_JointMotor2D;
    }

    private void OnReleased()
    {
        m_JointMotor2D.motorSpeed = -m_fSpeed;
        m_HingeJoint2D.motor = m_JointMotor2D;
    }
}
