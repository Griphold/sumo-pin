using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlayerLivesUI : MonoBehaviour
{
    [Inject] private SignalBus m_SignalBus;
    //[Inject] private PlayerLives m_PlayerLives;

    [SerializeField] private GameObject m_goBallOnUI;
    [SerializeField] private RectTransform m_rectUIToPopulate;

    private List<GameObject> m_ListOfBallsOnUI = new List<GameObject>();
    
    private void Awake()
    {
        m_SignalBus.Subscribe<PlayerLives.LivesChangedSignal>(OnLivesChanged);
    }

    private void PopulateUI(int _iCount)
    {
        for (int i = 0; i < _iCount; i++)
        {
            GameObject NewBallUI = Instantiate(m_goBallOnUI, m_rectUIToPopulate);
            m_ListOfBallsOnUI.Add(NewBallUI);
        }
    }

    private void OnLivesChanged(PlayerLives.LivesChangedSignal _signal)
    {
        //populate ui if neccessary
        if (m_ListOfBallsOnUI.Count < _signal.iCurrentLives)
        {
            PopulateUI(_signal.iCurrentLives);
        }
        else if (m_ListOfBallsOnUI.Count > _signal.iCurrentLives)
        {
            GameObject goBallOnUIToDestroy = m_ListOfBallsOnUI[m_ListOfBallsOnUI.Count - 1];
            
            Destroy(goBallOnUIToDestroy);

            m_ListOfBallsOnUI.Remove(goBallOnUIToDestroy);
        }
    }
}
