using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

public class GameOverUI : MonoBehaviour
{
    [Inject] private SignalBus m_SignalBus;
    [Inject] private PointCounter m_PointCounter;

    [SerializeField] private GameObject m_goGameOverUI;
    [SerializeField] private TMP_Text m_PointsText;

    #region Signals

    public class GameOverSignal
    {
        public GameOverSignal() { }
    }

    public class RestartSignal
    {
        public RestartSignal() { }
    }

    #endregion

    private void Awake()
    {
        m_SignalBus.Subscribe<GameOverSignal>(OnGameOver);
        m_goGameOverUI.SetActive(false);
    }

    public void OnRestart()
    {
        m_goGameOverUI.SetActive(false);
        m_SignalBus.Fire<RestartSignal>();
    }
    
    private void OnGameOver()
    {
        m_goGameOverUI.SetActive(true);
        
        //Set points from point counter
        m_PointsText.SetText("You scored: " + m_PointCounter.iPoints + " points!");
    }
}
