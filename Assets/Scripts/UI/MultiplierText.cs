using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

public class MultiplierText : MimiBehaviour
{
    [Inject] private SignalBus m_SignalBus;

    [SerializeField] private TMP_Text m_UIText;
    
    protected override void Awake()
    {
        base.Awake();
        
        m_SignalBus.Subscribe<PointCounter.MultiplierChangedSignal>(UpdateText);
    }

    private void UpdateText(PointCounter.MultiplierChangedSignal _signal)
    {
        m_UIText.SetText("x" + _signal.iMultiplier);
    }
}
