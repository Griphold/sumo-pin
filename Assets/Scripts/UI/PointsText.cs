using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

public class PointsText : MimiBehaviour
{
    [Inject] private SignalBus m_SignalBus;

    [SerializeField] private TMP_Text m_UIText;
    
    protected override void Awake()
    {
        base.Awake();
        
        m_SignalBus.Subscribe<PointCounter.PointsChangedSignal>(UpdateText);
    }

    private void UpdateText(PointCounter.PointsChangedSignal _signal)
    {
        m_UIText.SetText(_signal.iPoints.ToString());
    }
}
