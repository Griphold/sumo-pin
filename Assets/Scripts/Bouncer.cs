using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(Collider2D))]
public class Bouncer : MimiBehaviour
{
   [Inject] private SignalBus m_SignalBus;

   [SerializeField] private float m_fMinimumSpeedAfterBounce = 3;
   [SerializeField] private float m_fBounceSpeedMultiplier;
   [SerializeField] private int m_iAddedPointMultiplier;

   [SerializeField] private Animator m_AnimatorThis;
   
   protected override void Awake()
   {
      base.Awake();

      m_AnimatorThis = GetComponent<Animator>();
   }

   protected void OnCollisionEnter2D(Collision2D _colli2DOther)
   {
      //Make the ball faster when colliding with this 
      if (_colli2DOther.gameObject.CompareTag("Ball"))
      {
         //Speed up the ball when it is slower than the minimum speed
         //multiply the velocity if the ball is faster already
         Vector2 v2Velocity = _colli2DOther.rigidbody.velocity;
         if (v2Velocity.sqrMagnitude < m_fMinimumSpeedAfterBounce * m_fMinimumSpeedAfterBounce)
         {
            v2Velocity = v2Velocity.normalized * m_fMinimumSpeedAfterBounce;
         }
         else
         {
            v2Velocity *= m_fBounceSpeedMultiplier;
         }
         _colli2DOther.rigidbody.velocity = v2Velocity;
         
         //Add appropriate multiplier
         m_SignalBus.Fire(new PointCounter.MultiplierAddedSignal(m_iAddedPointMultiplier));
         
         AnimateBounce();
      }
   }

   private void AnimateBounce()
   {
      m_AnimatorThis.SetTrigger("Bounce");
   }
}
