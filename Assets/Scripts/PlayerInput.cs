using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private string m_sLeftButtonName;
    [SerializeField] private string m_sRightButtonName;
    [SerializeField] private string m_sFireButtonName;

    [Inject] private SignalBus m_SignalBus;
    
    #region Signals
    public class LeftActionSignal
    {
        public LeftActionSignal() { }
    }

    public class LeftActionReleasedSignal
    {
        public LeftActionReleasedSignal() { }
    }

    public class RightActionSignal
    {
        public RightActionSignal() { }
    }
    
    public class RightActionReleasedSignal
    {
        public RightActionReleasedSignal() { }
    }

    public class FireActionSignal
    {
        public FireActionSignal() { }
    }

    public class FireActionReleasedSignal
    {
        public FireActionReleasedSignal() { }
    }
    #endregion


    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown(m_sLeftButtonName))
        {
            m_SignalBus.Fire(new LeftActionSignal());
        }
        else if(Input.GetButtonUp(m_sLeftButtonName))
        {
            m_SignalBus.Fire<LeftActionReleasedSignal>();
        }
        
        if (Input.GetButtonDown(m_sRightButtonName))
        {
            m_SignalBus.Fire(new RightActionSignal());
        }
        else if(Input.GetButtonUp(m_sRightButtonName))
        {
            m_SignalBus.Fire<RightActionReleasedSignal>();
        }

        if (Input.GetButtonDown(m_sFireButtonName))
        {
            m_SignalBus.Fire(new FireActionSignal());
        }
        else if(Input.GetButtonUp(m_sFireButtonName))
        {
            m_SignalBus.Fire(new FireActionReleasedSignal());
        }
    }
}
