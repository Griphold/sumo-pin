using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(Collider2D))]
public class PointZone : MonoBehaviour
{
    [Inject] private SignalBus m_SignalBus;

    [SerializeField] private int m_iPoints;

    protected void OnTriggerEnter2D(Collider2D _col2DOther)
    {
        //Add some points when the ball reaches this zone
        if (_col2DOther.gameObject.CompareTag("Ball"))
        {
            m_SignalBus.Fire(new PointCounter.PointsAddedSignal(m_iPoints));
        }
    }
}
