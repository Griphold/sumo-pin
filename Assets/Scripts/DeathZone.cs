using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(Collider2D))]
public class DeathZone : MonoBehaviour
{
    [Inject] private SignalBus m_SignalBus;

    [SerializeField] private int m_iBallKillDelay;
    [SerializeField] private int m_iBallSpawnDelay;
    
    private void OnTriggerEnter2D(Collider2D _col2DOther)
    {
        if (_col2DOther.CompareTag("Ball"))
        {
            Destroy(_col2DOther.gameObject, m_iBallKillDelay);
            StartCoroutine(SignalBallSpawn());
        }
    }

    private IEnumerator SignalBallSpawn()
    {
        yield return new WaitForSeconds(m_iBallSpawnDelay);

        m_SignalBus.Fire<PlayerLives.BallDestroyedSignal>();
    }
}
