using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlayerLives : MonoBehaviour
{
    [Inject] private SignalBus m_SignalBus;
    
    [SerializeField] private int m_iStartingBalls = 3;
    [SerializeField] private GameObject m_goBallPrefab;
    [SerializeField] private Transform m_transBallSpawn;

    private int m_iRemainingBalls;

    #region Signals

    public class BallDestroyedSignal
    {
        public BallDestroyedSignal() { }
    }

    public class LivesChangedSignal
    {
        private int m_iCurrentLives;
        public LivesChangedSignal(int mICurrentLives)
        {
            m_iCurrentLives = mICurrentLives;
        }

        public int iCurrentLives
        {
            get => m_iCurrentLives;
        }
    }

    #endregion
    
    private void Awake()
    {
        //Subscribe to appropriate signals
        m_SignalBus.Subscribe<BallDestroyedSignal>(TrySpawnBall);
        m_SignalBus.Subscribe<GameOverUI.RestartSignal>(RestartGame);
        
        RestartGame();
    }

    private void Start()
    {
        m_SignalBus.Fire(new LivesChangedSignal(m_iRemainingBalls));
    }

    private void TrySpawnBall()
    {
        //Check lives
        if (m_iRemainingBalls == 0)
        {
            //Game over!
            m_SignalBus.Fire<GameOverUI.GameOverSignal>();
        }
        else
        {
            Instantiate(m_goBallPrefab, m_transBallSpawn.position, m_transBallSpawn.rotation);
            m_iRemainingBalls -= 1;
            m_SignalBus.Fire(new LivesChangedSignal(m_iRemainingBalls));
        }
    }

    private void RestartGame()
    {
        m_iRemainingBalls = m_iStartingBalls;

        TrySpawnBall();
    }
}
