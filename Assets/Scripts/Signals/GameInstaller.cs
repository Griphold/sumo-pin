using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [SerializeField] private PointCounter m_PointCounter;
    [SerializeField] private PlayerLives m_PlayerLives;
    
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);

        //Input signals
        Container.DeclareSignal<PlayerInput.LeftActionSignal>();
        Container.DeclareSignal<PlayerInput.LeftActionReleasedSignal>();
        Container.DeclareSignal<PlayerInput.RightActionSignal>();
        Container.DeclareSignal<PlayerInput.RightActionReleasedSignal>();
        Container.DeclareSignal<PlayerInput.FireActionSignal>();
        Container.DeclareSignal<PlayerInput.FireActionReleasedSignal>();
        
        //Point Signals
        Container.DeclareSignal <PointCounter.PointsChangedSignal>();
        Container.DeclareSignal<PointCounter.PointsAddedSignal>();
        Container.DeclareSignal<PointCounter.MultiplierChangedSignal>();
        Container.DeclareSignal<PointCounter.MultiplierAddedSignal>();
        
        //Other Signals
        Container.DeclareSignal<PlayerLives.BallDestroyedSignal>();
        Container.DeclareSignal<PlayerLives.LivesChangedSignal>();
        Container.DeclareSignal<GameOverUI.GameOverSignal>();
        Container.DeclareSignal<GameOverUI.RestartSignal>();

        //Bind scene managers
        Container.BindInterfacesAndSelfTo<PointCounter>().FromInstance(m_PointCounter).AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerLives>().FromInstance(m_PlayerLives).AsSingle();
    }
}
